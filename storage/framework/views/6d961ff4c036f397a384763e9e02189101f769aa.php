<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <li class="m-menu__item  m-menu__item--submenu <?php echo e(areActiveRoutes(["clients.index"])); ?>"
                aria-haspopup="true">
                <a href="<?php echo e(route('clients.index')); ?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text">
                        Clients
                    </span>
                    
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php echo e(areActiveRoutes(["transactions.index"])); ?>"
                aria-haspopup="true">
                <a href="<?php echo e(route('transactions.index')); ?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-graphic-2"></i>
                    <span class="m-menu__link-text">
                        Transaction
                    </span>
                    
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php echo e(areActiveRoutes(["client_transactions.index"])); ?>"
                aria-haspopup="true">
                <a href="<?php echo e(route('client_transactions.index')); ?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-graphic-2"></i>
                    <span class="m-menu__link-text">
                        Client Transaction
                    </span>
                    
                </a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<?php /**PATH C:\wamp64\www\client_app1\resources\views/backend/layouts/sidebar/side_bar.blade.php ENDPATH**/ ?>