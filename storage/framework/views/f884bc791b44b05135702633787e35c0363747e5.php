
<?php $__env->startSection('content'); ?>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title">
                        <?php echo (isset($client_transactions))? 'Edit Client Transaction' : 'Add Client Transaction'; ?>

                    </h3>
                        <div><?php echo (isset($client_transactions)) ? 'Interest Payment Pending :: '.date('d-m-Y',strtotime($client_transactions->pending_payment_date)) : '';; ?></div>

                </div>
                    <div class="row align-items-center">
                        <div class="col-xl-4 ">
                            <h4><?php echo e($transaction->client->name); ?></h4>
                            <?php if( $transaction->with_interest == 'Yes'): ?>
                                <span> Interest Rate - <?php echo e(intval($transaction->interest_rate)); ?> %</span>
                            <?php else: ?>
                                <span>No Interest</span>
                            <?php endif; ?>
                        </div>
                        <div class="col-xl-4 ">
                            <h4>Status : <?php echo e($transaction->status); ?> </h4>
                           
                        </div>
                        <div class="col-xl-4 ">
                            <h4>Due Amount : </h4>
                            <?php if( !empty($pending_interests_amount )): ?>
                                <h6 style="color: red;"><?php echo e(($transaction->pending_amount + $pending_interests_amount)); ?></h6>
                            <?php else: ?>
                                <h6 style="color: red;"><?php echo e($transaction->pending_amount); ?></h6>

                            <?php endif; ?>
                        </div>

                    </div>

            </div>
        </div>

        <div class="m-content" style="width: 100%">
            <div class="m-portlet m-portlet--tab">

                <form id="transactionForm" class="m-form m-form--fit m-form--label-align-right"
                      action="<?php echo isset($client_transactions) ? route('client_transactions.update', $client_transactions->id) : route('client_transactions.store',$transaction_id); ?>"
                      method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <?php if(isset($client_transactions)): ?>
                        <?php echo method_field('PUT'); ?>
                    <?php endif; ?>
                    <input type="hidden" name="transaction_id" value="<?php echo (isset($client_transactions)) ? $client_transactions->transaction_id : $transaction_id; ?>"/>
                    <div class="m-portlet__body">

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Amount*
                                    </label>
                                    <?php if(isset($client_transactions)): ?>
                                        <h5><?php echo (isset($client_transactions)) ? $client_transactions->amount :''; ?></h5>
                                    <?php else: ?>
                                        <input type="text" class="form-control m-input" name="amount"
                                               placeholder="Enter Amount"
                                               value="<?php echo isset($client_transactions) ? $client_transactions->amount : old('amount'); ?>">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Payment Mode*
                                    </label>
                                    <div class="m-radio-inline">
                                        <label class="m-radio">
                                            <input type="radio" name="payment_mode" class=""  value="Cash" <?php echo (old('payment_mode') != 'Bank Transfer') ?'CHECKED' : ''; ?>> Cash
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="payment_mode" class="" value="Bank Transfer" <?php echo (old('payment_mode') == 'Bank Transfer') ?'CHECKED' : ''; ?>> Bank Transfer
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="form-control-label">
                                        Payment Type*
                                    </label>
                                    <div class="m-radio-inline">
                                        <?php if($transaction->with_interest == 'Yes'): ?>
                                                <label class="m-radio">
                                                    <input type="radio" name="payment_type" class=""  value="Interest Amount" <?php echo ((!empty($client_transactions) && $client_transactions->payment_type == 'Interest Amount') || old('payment_type') == 'Interest Amount' || ( $transaction->with_interest == 'Yes' &&  old('payment_type') != 'Primary Amount')) ?'CHECKED' : ''; ?> > Interest Amount
                                                    <span></span>
                                                </label>
                                        <?php endif; ?>
                                        <label class="m-radio">
                                            <input type="radio" name="payment_type" class="" value="Primary Amount" <?php echo ((!empty($client_transactions) && $client_transactions->payment_type == 'Primary Amount') || ( $transaction->with_interest == 'No' ) || old('payment_type') == 'Primary Amount' )?'CHECKED' : ''; ?> <?php echo isset($client_transactions) ? 'disabled' :''; ?>> Primary Amount
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="form-control-label">
                                        Date *
                                    </label>
                                    <input type="text" class="form-control m-input" name="payment_date" id="m_datepicker_1"
                                           placeholder=""
                                           value="<?php echo (isset($client_transactions)) ? date('m/d/Y',strtotime($client_transactions->payment_date)) :old('payment_date'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="form-control-label">
                                        Note *
                                    </label>
                                    <textarea  class="form-control m-input" name="note" id="" rows="3"><?php echo (isset($client_transactions)) ? $client_transactions->note :''; ?></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <a href="<?php echo e(route('transactions.index')); ?>" class="btn btn-secondary">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('theme/js/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#transactionForm').validate({
                ignore: "input[type='text']:hidden",
                rules: {
                    transaction_id: "required",
                    amount: {
                        required: true,
                        number: true
                    },
                    payment_mode :'required',
                    payment_type :'required',

                },
                messages: {
                    amount: {required:"Amount field is required",number:"Only Numbers are allowed"},
                    payment_mode :'Payment Mode required',
                    payment_type :'Payment Type required',
                },
                submitHandler: function (e) {
                    e.submit();
                }
            });

            $( ".interestOption" ).on( "click", function () {
                var interestOption = $(".interestOption:checked").val();
                if(interestOption == 'Yes') {
                    $('.IneterestDiv').show();
                }
                else {
                    $('.IneterestDiv').hide();
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\client_app1\resources\views/backend/client_transactions/form.blade.php ENDPATH**/ ?>