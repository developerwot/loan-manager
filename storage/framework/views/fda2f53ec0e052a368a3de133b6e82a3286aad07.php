
<?php $__env->startSection('content'); ?>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title">
                        <?php echo e((isset($transaction))? 'Edit Transaction' : 'Add Transaction'); ?>

                    </h3>
                </div>
            </div>
        </div>

        <div class="m-content" style="width: 100%">
            <div class="m-portlet m-portlet--tab">
                <form id="transactionForm" class="m-form m-form--fit m-form--label-align-right"
                      action="<?php echo e(isset($transaction) ? route('transactions.update', $transaction->id) : route('transactions.store')); ?>"
                      method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <?php if(isset($transaction)): ?>
                        <?php echo method_field('PUT'); ?>
                    <?php endif; ?>
                    <div class="m-portlet__body">

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Select Client *
                                    </label>
                                    <select class="form-control m-input m_selectpicker" name="user_id" data-live-search="true">
                                        <option value="" disabled selected>Select Client</option>
                                        <?php if(isset($clients) &&  count($clients) > 0): ?>
                                            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option
                                                        value="<?php echo e($client->id); ?>" <?php if( (isset($transaction) && $transaction->user_id == $client->id ) || old('user_id') == $client->id ): ?> selected <?php endif; ?>>
                                                    <?php echo e($client->name); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Amount*
                                    </label>
                                    <input type="text" class="form-control m-input" name="amount"
                                           placeholder="Enter Amount"
                                           value="<?php echo isset($transaction) ? $transaction->amount : old('amount'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        With Interest*
                                    </label>
                                    <div class="m-radio-list">
                                        <label class="m-radio">
                                            <input type="radio" name="with_interest" class="interestOption"  value="Yes" <?php echo ((isset($transaction) && $transaction->with_interest == 'Yes') || (old('with_interest') == 'Yes'))?'CHECKED' : ''; ?>  > Yes
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="with_interest" class="interestOption" value="No" <?php echo ((isset($transaction) && $transaction->with_interest == 'No') || (!isset($transaction) && old('with_interest') != 'Yes'))?'CHECKED' : ''; ?> > No
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 IneterestDiv" style="<?php echo ((isset($transaction) && $transaction->with_interest == 'Yes') || old('with_interest') == 'Yes') ?'display: block;' :'display: none;'; ?> ">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                         Interest Rate*
                                    </label>
                                    <input type="text" class="form-control m-input" name="interest_rate"
                                           placeholder="Enter Interest Rate"
                                           value="<?php echo isset($transaction) ? $transaction->interest_rate : old('interest_rate'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row IneterestDateDiv" style="<?php echo ((isset($transaction) && $transaction->with_interest == 'Yes') || old('with_interest') == 'Yes')  ?'display: flex;' :'display: none;'; ?> ">
                           <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Interest Payment Date From* (Day of Month)
                                    </label>

                                    <select name="dom_from" class="form-control m-input col-lg-3">
                                        <?php $__currentLoopData = range(1, 30, 1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option  value="<?php echo e($i); ?>" <?php if((isset($transaction) && $transaction->dom_from == $i) || old('dom_from') == $i ): ?> selected <?php endif; ?>><?php echo e($i); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                           </div>
                            <div class="col-lg-6">
                               <div class="form-group m-form__group ">
                                   <label class="">
                                       Interest Payment  Date To* (Day of Month)
                                   </label>
                                    <select name="dom_to" class="form-control m-input col-lg-3">
                                        <?php $__currentLoopData = range(1, 30, 1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($i); ?>" <?php if((isset($transaction) && $transaction->dom_to == $i ) || old('dom_to') == $i ): ?> selected <?php endif; ?>><?php echo e($i); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                           </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Payment Mode*
                                    </label>
                                    <div class="m-radio-list">
                                        <label class="m-radio">
                                            <input type="radio" name="payment_mode" class=""  value="Cash"  <?php echo ((isset($transaction) && $transaction->payment_mode == 'Cash') || (!isset($transaction) && old('payment_mode') != 'Bank Transfer'))?'CHECKED' : ''; ?>> Cash
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="payment_mode" class="" value="Bank Transfer" <?php echo ((!empty($transaction) && $transaction->payment_mode == 'Bank Transfer') || (!isset($transaction) && old('payment_mode') == 'Bank Transfer') ) ?'CHECKED' : ''; ?>> Bank Transfer
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                       Transaction Date*
                                    </label>
                                    <input type="text" class="form-control m-input" name="transaction_date" id="m_datepicker_1"
                                           autocomplete="off"  placeholder="<?php echo (!empty($transaction))? date('m/d/Y',strtotime($transaction->transaction_date)) : ''; ?>"
                                           value="<?php echo (!empty($transaction))? date('m/d/Y',strtotime($transaction->transaction_date)) : old('transaction_date'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label class="">
                                        Note*
                                    </label>
                                    <textarea  class="form-control m-input" name="note" id="" rows="3"><?php echo (!empty($transaction))?$transaction->note : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <a href="<?php echo e(route('transactions.index')); ?>" class="btn btn-secondary">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('theme/js/bootstrap-daterangepicker.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('theme/js/bootstrap-select.js')); ?>" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#transactionForm').validate({
                ignore: "input[type='text']:hidden",
                rules: {
                    user_id: "required",
                    amount: {
                        required: true,
                        number: true
                    },
                    with_interest:'required',
                    interest_rate: {
                        required: true,
                        number: true
                    },
                    dom_from:'required',
                    dom_to:'required',
                    payment_mode :'required',
                    note:'required'

                },
                messages: {
                    user_id: "Please select any Client",
                    amount: {required:"Amount field is required",number:"Only Numbers are allowed"},
                    with_interest: {
                        required: "With interest Field is required",
                    },
                    interest_rate :  {
                        required: 'Interest Rate required',
                        number:'Only Numbers are allowed'
                    },
                    date_from_to:'Select Date rage'
                },
                submitHandler: function (e) {
                    e.submit();
                }
            });

            $( ".interestOption" ).on( "click", function () {
                var interestOption = $(".interestOption:checked").val();
                if(interestOption == 'Yes') {
                    $('.IneterestDiv').show();
                    $('.IneterestDateDiv').show();
                    console.log("in");
                    $("#m_daterangepicker_2_validate").daterangepicker
                    ({buttonClasses:"m-btn btn",applyClass:"btn-primary",
                        cancelClass:"btn-secondary"},function(a,t,n)
                                    {
                                        console.log(a);
                                        $("#m_daterangepicker_3_validate .form-control").val(a.format("YYYY-MM-DD")+" / "+t.format("YYYY-MM-DD"))
                                    }
                                    )

                }
                else {
                    $('.IneterestDiv').hide();
                    $('.IneterestDateDiv').hide();
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\client_app1\resources\views/backend/transactions/form.blade.php ENDPATH**/ ?>