@extends('backend.layouts.master')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Client Transaction
                    </h3>
                </div>
            </div>
        </div>


        <div class="m-content">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-3 order-2 order-xl-1">
                                <h4>{{ $transaction->client->name }}</h4>
                                @if( $transaction->with_interest == 'Yes')
                                    <span> Interest Rate - {{ intval($transaction->interest_rate) }} %</span>
                                @else
                                    <span>No Interest</span>
                                @endif
                            </div>
                            <div class="col-xl-3 order-2 order-xl-1">
                                <h4>Status : {{ $transaction->status }} </h4>
                               {{-- <span> Note - {{ $transaction->note}} </span>--}}
                            </div>
                            <div class="col-xl-3 order-2 order-xl-1">
                                <h4>Due Amount : </h4>
                                @if( !empty($pending_interests_amount ))
                                    <h6 style="color: red;">{{ ($transaction->pending_amount + $pending_interests_amount)}}</h6>
                                @else
                                    <h6 style="color: red;">{{ $transaction->pending_amount}}</h6>

                                @endif
                            </div>
                            @if($transaction->status == 'Ongoing')
                                <div class="col-xl-3 order-1 order-xl-2 m--align-right">
                                    <a href="{{ route('client_transactions.create',$transaction->id) }}"
                                       class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                                    <span>
                                                        <i class="la flaticon-add-circular-button"></i>
                                                        <span>
                                                            Add Client Payment
                                                        </span>
                                                    </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive" >
                        <table class="table  table-hover" id="ctransactionTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Payment Date </th>
                                    <th>Interest Month </th>
                                    <th>Payment Type</th>
                                    <th>Payment Mode</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $rowcount = 1; @endphp

                            @if(!empty($transaction->client_transaction))
                                @foreach($transaction->client_transaction as $ctrans)
                                    <tr>
                                        <td>{{$rowcount}}</td>
                                        <td>{!! ($ctrans->payment_status == 'Paid') ? \Carbon\Carbon::parse($ctrans->payment_date)->format('d-m-Y'): '-' !!} </td>
                                        <td>{!! ($ctrans->payment_type == 'Interest Amount' && isset($ctrans->pending_payment_date)) ? \Carbon\Carbon::parse($ctrans->pending_payment_date)->format('d-m-Y') : '-' !!} </td>
                                        <td>
                                            @if($ctrans->payment_type == 'Primary Amount')
                                                <button class="btn m-btn--square  btn-outline-success btn-sm active" style="cursor: default">{{ $ctrans->payment_type }} </button>
                                             @else
                                                <button class="btn m-btn--square btn-outline-info btn-sm active" style="cursor: default">{{ $ctrans->payment_type }} </button>
                                             @endif
                                        </td>
                                        <td>{{ $ctrans->payment_mode }}</td>
                                        <td>
                                            @if( $ctrans->payment_status  == 'Pending')
                                                <button class="btn m-btn--square  btn-outline-danger btn-sm active" style="cursor: default">{{ $ctrans->payment_status }} </button>
                                            @else
                                                <button class="btn m-btn--square  btn-outline-primary btn-sm active" style="cursor: default"> {{ $ctrans->payment_status }} </button>
                                            @endif
                                            </td>
                                        <td style="{!! $ctrans->payment_status  == 'Pending'?'color:red':'color:blue' !!};">- {{ $ctrans->amount }}</td>
                                        <td>

                                                <a href="{!! route('client_transactions.edit',$ctrans->id)!!}"
                                                class="edit-button m-portlet__nav-link btn m-btn
                                                m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Add Payment">
                                                                    <i class="la la-edit"></i>
                                                </a>

                                                <a href="javascript;;"
                                                   class="edit-button m-portlet__nav-link btn m-btn
                                                m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Note" data-toggle="modal" data-target="#m_modal_{!! $ctrans->id !!}">
                                                    <i class="la la-sticky-note"></i>
                                                </a>
                                                <div class="modal fade" id="m_modal_{!! $ctrans->id !!}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Note</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>{{ $ctrans->note }}</p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        @php $rowcount++; @endphp
                                    </tr>
                                @endforeach
                            @endif
                            @if(!empty($transaction))

                                    <tr>
                                        <td>{{$rowcount}}</td>
                                        <td>{{ \Carbon\Carbon::parse($transaction->transaction_date)->format('d-m-Y')}}</td>
                                        <td>-</td>
                                        <td><button class="btn m-btn--square  btn-outline-success btn-sm active" style="cursor: default">Primary Amount </button></td>
                                        <td>{{ $transaction->payment_mode }}</td>
                                        <td>-</td>
                                        <td style="color: green;background: aquamarine;">+ {{ $transaction->amount }}</td>
                                        <td></td>
                                    </tr>
                                    @php $rowcount++; @endphp
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="deleteForm" method="post" action="" style="display: none;">
        @csrf
        @method('delete')
    </form>
    @include('backend.snippets.delete')
@endsection



@section('scripts')
    <script>

       /* $('#ctransactionTable').mDatatable({ "columnDefs": [
                { "orderable": false, "targets": 7 },
            ]  ,responsive:!0,});*/
        /* $(document).on('click', '.delete-button-action', function (e) {
             $('#myModal').modal();
             $('#delete_btn').attr('href', '{{route('client_transactions.index')}}/delete/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('client_transactions.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });*/

        var client_transactions = {!! $transaction->client_transaction !!};
        //  console.log('client_transactions', client_transactions);


      /*  var DatatableDataLocalDemo = {
            init: function () {
                var e, a, i;
                e = client_transactions, a = $(".m_datatable").mDatatable({
                    data: {
                        type: "local",
                        source: e,
                        pageSize: 10
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#generalSearch")},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 50,
                        sortable: 1,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "payment_date",
                        title: "Date",
                        width: 100,
                        sortable: 1,
                        // textAlign: "center",
                    }, {
                        field: "amount",
                        title: "Amount",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }, {
                        field: "payment_mode",
                        title: "Payment Mode",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "payment_type",
                        title: "Payment Type",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",
                        template: function (e, a, i) {

                            var data = '';
                            if(e.payment_type == 'Primary Amount')
                                data += '<button class="btn m-btn--square  btn-outline-success btn-sm active" style="cursor: default"> ' + e.payment_type + ' </button>';
                            else
                                data += '<button class="btn m-btn--square  btn-outline-info btn-sm active" style="cursor: default"> ' + e.payment_type + ' </button>';
                            return data;

                        }
                        // textAlign: "center",
                    },{
                        field: "transaction_date",
                        title: "Date",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }, /!* {
                        field: "Actions",
                        width: 150,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            var edit = '\t\t\t\t\t\t<a href="javascript:void(0);" data-id="' + e.id + '" ' +
                                'class="edit-button m-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">        ' +
                                '                    <i class="la la-edit"></i>                        </a>\t\t\t\t\t';
                            var deleteB = '\t\t\t\t\t\t<a href="javascript:void(0);" class="delete-button-action ' +
                                'm-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-danger m-btn--icon m-btn--icon-only ' +
                                'm-btn--pill" title="Delete " data-id="' + e.id + '">                            <i ' +
                                'class="fa fa-trash-o"' +
                                ' ' +
                                '></i>                        </a>\t\t\t\t\t';


                            return edit + '\t\t' + deleteB;
                        }
                    }*!/]
                }), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
                    a.search($(this).val(), "Status")
                }).val(void 0 !== i.Status ? i.Status : ""), $("#m_form_type").on("change", function () {
                    a.search($(this).val(), "Type")
                }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemo.init();

        });*/
    </script>
@endsection
