@extends('backend.layouts.master')
@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Transactions
                    </h3>
                </div>
            </div>
        </div>


        <div class="m-content">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid"
                                                   placeholder="Search" id="generalSearchCustom">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="{{ route('transactions.create') }}"
                                   class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la flaticon-add-circular-button"></i>
													<span>
														Create Transaction
													</span>
												</span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <div class="transaction_datatable" id="child_data_local"></div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($transactions))
        @foreach($transactions as $transaction)
            <div class="modal fade" id="m_modal_{!! $transaction->id !!}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Note</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>{{ $transaction->note }}</p>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @endif
    <form id="deleteForm" method="post" action="" style="display: none;">
        @csrf
        @method('delete')
    </form>
    @include('backend.snippets.delete')
@endsection



@section('scripts')
    <script>

        $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('transactions.index')}}/delete/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('transactions.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });
        $(document).on('click', '.add-client-trans-button', function (e) {
            var trans_editUrl = '{{route('client_transactions.create','')}}/' + $(this).data('id') ;
           console.log(trans_editUrl);
            location.href = trans_editUrl;
        });
        $(document).on('click', '.view_transaction', function (e) {
            var trans_editUrl = '{{route('transactions.show','')}}/' + $(this).data('id') ;
           console.log(trans_editUrl);
            location.href = trans_editUrl;
        });

        var transactions = {!! $transactions !!};
        console.log('transactions', transactions);


        var DatatableDataMyDemo = {
            init: function () {
                var e, a, i;
                e = transactions, a = $(".transaction_datatable").mDatatable({

                    data: {
                        type: "local",
                        source: e,
                        pageSize: 10
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    "order": [[ 0, "desc" ]],
                    pagination: !0,
                    search: {input: $("#generalSearchCustom")},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 50,
                        sortable: !1,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "name",
                        title: "Name",
                        width: 70,
                        sortable: 1,
                        // textAlign: "center",
                    }, {
                        field: "amount",
                        title: "Amount",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "pending_amount",
                        title: "Due Amt",
                        width: 70,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "with_interest",
                        title: "Interest",
                        width: 70,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }
                    ,{
                        field: "interest_rate",
                        title: "Int Rate",
                        width: 50,
                            sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "transaction_date",
                        title: "Date",
                        width: 100,
                            sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "status",
                        title: "Status",
                        width: 80,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }, {
                        field: "Actions",
                        width: 200,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            var note = '\t\t\t\t\t\t<a href="javascript;;"  data-toggle="modal" ' + 'data-target="#m_modal_' + e.id + '"' +
                                'class="m-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Note">        ' +
                                '                    <i class="la la-sticky-note"></i>                        </a>\t\t\t\t\t';

                            var edit = '\t\t\t\t\t\t<a href="javascript:void(0);" data-id="' + e.id + '" ' +
                                'class="edit-button m-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">        ' +
                                '                    <i class="la la-edit"></i>                        </a>\t\t\t\t\t';

                            var deleteB = '\t\t\t\t\t\t<a href="javascript:void(0);" class="delete-button-action ' +
                                'm-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-danger m-btn--icon m-btn--icon-only ' +
                                'm-btn--pill" title="Delete " data-id="' + e.id + '">                            <i ' +
                                'class="fa fa-trash-o"' +
                                ' ' +
                                '></i>                        </a>\t\t\t\t\t';

                            var view_transaction = '\t\t\t\t\t\t<a href="javascript:void(0);" data-id="' + e.id + '" ' +
                                'class="view_transaction m-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Show Client Transaction">        ' +
                                '                     <i class="la la-tasks"></i>                       </a>\t\t\t\t\t';


                            if(e.status == 'Ongoing') {

                                var add_transaction = '\t\t\t\t\t\t<a href="javascript:void(0);" data-id="' + e.id + '" ' +
                                    'class="add-client-trans-button m-portlet__nav-link ' +
                                    'btn m-btn ' +
                                    'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Add Client Payment">        ' +
                                    '                     <i class="la la-plus-circle"></i>                       </a>\t\t\t\t\t';
                                return note + '\t\t' + edit + '\t\t' + deleteB + '\t\t' + view_transaction + '\n\n' + add_transaction;
                            }
                            else {
                                return  note + '\t\t' +  deleteB + '\t\t' + view_transaction
                            }

                        }
                    }]
                }), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
                    a.search($(this).val(), "Status")
                }).val(void 0 !== i.Status ? i.Status : ""), $("#m_form_type").on("change", function () {
                    a.search($(this).val(), "Type")
                }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function () {
            DatatableDataMyDemo.init();

        });
    </script>
@endsection
