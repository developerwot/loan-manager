@extends('backend.layouts.master')
@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Client Transaction
                    </h3>
                </div>
            </div>
        </div>


        <div class="m-content">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid"
                                                   placeholder="Search" id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                {{--<a href="{{ route('client_transactions.create') }}"
                                   class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la flaticon-add-circular-button"></i>
													<span>
														Create Transaction
													</span>
												</span>
                                </a>--}}
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <div class="ct_datatable" id="child_data_local"></div>
                </div>
            </div>
        </div>
    </div>

    <form id="deleteForm" method="post" action="" style="display: none;">
        @csrf
        @method('delete')
    </form>
    @include('backend.snippets.delete')
@endsection



@section('scripts')
    <script>

       /* $(document).on('click', '.delete-button-action', function (e) {
            $('#myModal').modal();
            $('#delete_btn').attr('href', '{{route('client_transactions.index')}}/delete/' + $(this).data('id'));
        });

        $(document).on('click', '.edit-button', function (e) {
            var editUrl = '{{route('client_transactions.index')}}/' + $(this).data('id') + '/edit';
            location.href = editUrl;
        });*/

        var client_transactions = {!! $client_transactions !!};
          console.log('client_transactions', client_transactions);


        var DatatableDataLocalDemo = {
            init: function () {
                var e, a, i;
                e = client_transactions, a = $(".ct_datatable").mDatatable({
                    data: {
                        type: "local",
                        source: e,
                        pageSize: 10
                    },
                    layout: {theme: "default", class: "", scroll: !1, footer: !1},
                    sortable: !0,
                    pagination: !0,
                    search: {input: $("#generalSearch")},
                    columns: [{
                        field: "id",
                        title: "#",
                        width: 50,
                        sortable: 1,
                        textAlign: "center",
                        template: function (e, a, i) {
                            return  a + 1;
                        }
                    },  {
                        field: "client_name",
                        title: "Name",
                        width: 100,
                        sortable: 1,
                        // textAlign: "center",
                    }, {
                        field: "amount",
                        title: "Amount",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }, {
                        field: "payment_mode",
                        title: "Payment Mode",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    },{
                        field: "payment_type",
                        title: "Payment Type",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",
                        template: function (e, a, i) {

                                var data = '';
                                if(e.payment_type == 'Primary Amount')
                                    data += '<button class="btn m-btn--square  btn-outline-success btn-sm active" style="cursor: default"> ' + e.payment_type + ' </button>';
                                else
                                    data += '<button class="btn m-btn--square  btn-outline-info btn-sm active" style="cursor: default"> ' + e.payment_type + ' </button>';
                                return data;

                        }
                        // textAlign: "center",
                    },{
                        field: "payment_status",
                        title: "Payment Status",
                        width: 150,
                        sortable: 1,
                        overflow: "visible",
                        template: function (e, a, i) {

                                var data = '';
                                if(e.payment_status == "Pending")
                                    data += '<button class="btn m-btn--square  btn-outline-danger btn-sm active" style="cursor: default"> ' + e.payment_status + ' </button>';
                                else
                                    data += '<button class="btn m-btn--square  btn-outline-primary btn-sm active" style="cursor: default"> ' + e.payment_status + ' </button>';
                                return data;

                        }
                        // textAlign: "center",
                    },{
                        field: "transaction_date",
                        title: "Date",
                        width: 100,
                        sortable: 1,
                        overflow: "visible",
                        // textAlign: "center",
                    }, /* {
                        field: "Actions",
                        width: 150,
                        title: "Action",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, a, i) {
                            var edit = '\t\t\t\t\t\t<a href="javascript:void(0);" data-id="' + e.id + '" ' +
                                'class="edit-button m-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">        ' +
                                '                    <i class="la la-edit"></i>                        </a>\t\t\t\t\t';
                            var deleteB = '\t\t\t\t\t\t<a href="javascript:void(0);" class="delete-button-action ' +
                                'm-portlet__nav-link ' +
                                'btn m-btn ' +
                                'm-btn--hover-danger m-btn--icon m-btn--icon-only ' +
                                'm-btn--pill" title="Delete " data-id="' + e.id + '">                            <i ' +
                                'class="fa fa-trash-o"' +
                                ' ' +
                                '></i>                        </a>\t\t\t\t\t';


                            return edit + '\t\t' + deleteB;
                        }
                    }*/]
                }), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
                    a.search($(this).val(), "Status")
                }).val(void 0 !== i.Status ? i.Status : ""), $("#m_form_type").on("change", function () {
                    a.search($(this).val(), "Type")
                }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function () {
            DatatableDataLocalDemo.init();

        });
    </script>
@endsection
