@extends('backend.layouts.auth_master')
@section('content')

    <div class="m-login__signin">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Sign In To Admin
            </h3>
        </div>
        <form id="form" class="m-login__form m-form" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group m-form__group">
                <input class="form-control m-input" type="text" placeholder="Email" name="email"
                       autocomplete="off" value="{{ old('email') }}">
            </div>
            <div class="form-group m-form__group">
                <input class="form-control m-input" type="password"
                       placeholder="Password" name="password">
            </div>
            <div class="row m-login__form-sub">
{{--                <div class="col m--align-left">--}}
{{--                    <label class="m-checkbox m-checkbox--focus">--}}
{{--                        <input type="checkbox" name="remember">--}}
{{--                        Remember me --}}
{{--                        <span></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
                <div class="col m--align-right">
                    <a href="{{ route('password.request') }}">
                        Forgotten Password ?
                    </a>
                </div>
            </div>
            <div class="m-login__form-action">
                <button type="submit"
                        class="btn btn-primary btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                    Sign In
                </button>
            </div>
        </form>

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            $('#form').validate({
                rules: {
                    email: {
                        email: true,
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },messages: {
                    email: {
                        required: "Email field is required",
                        email: "Please use valid email format"
                    },
                    password: {
                        required: "Password field is required",
                        minlength: "Please enter at least 6 characters."
                    }
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
