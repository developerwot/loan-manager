@extends('backend.layouts.auth_master')
@section('content')

    <div class="m-login__signin">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Reset Password
            </h3>
        </div>

        <form id="form" class="m-login__form m-form" action="{{ route('password.reset') }}" method="POST">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
{{--            <div class="form-group m-form__group">--}}
{{--                <input class="form-control m-input" type="text" placeholder="@lang('messages.placeholder_email')" name="email" autocomplete="off"--}}
{{--                       value="{{ $email ?? old('email') }}">--}}
{{--            </div>--}}
            <div class="form-group m-form__group">
                <input class="form-control m-input" type="password" placeholder="Password" name="password"
                       id="password">
            </div>
            <div class="form-group m-form__group">
                <input class="form-control m-input" type="password" placeholder="Confirm password here"
                       name="password_confirmation">
            </div>
            <div class="m-login__form-action">
                <button type="submit" class="btn btn-primary btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                    Reset Password
                </button>
            </div>
        </form>

    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });

            $('#form').validate({
                rules: {
                    email: {
                        email: true,
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#password"
                    }

                },messages: {
                    email: {
                        required: "Email field is required",
                        email: "Please use valid email format"
                    },
                    password: {
                        required: "Password field is required",
                        minlength: "Please enter at least 6 characters."
                    }
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });

        });
    </script>
@endsection
