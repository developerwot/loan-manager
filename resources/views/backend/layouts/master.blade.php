<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        {{config('app.name')}}
    </title>

    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="shortcut icon" href="{{ asset('public/theme/icons/favicon.ico') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('theme/css/css.css') }}" media="all">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" media="all">
    <link href="{{ asset('theme/css/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/css/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://codeseven.github.io/toastr/build/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">

    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('theme/js/webfont.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

    {{--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>--}}

    {{--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <link rel="stylesheet" href="{{ asset('theme/css/css.css') }}" media="all">
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <script src="{{ asset('theme/js/jquery.mousewheel.min.js') }}"></script>
    <style type="text/css">
        span.im-caret {
            -webkit-animation: 1s blink step-end infinite;
            animation: 1s blink step-end infinite;
        }

        @keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        @-webkit-keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        span.im-static {
            color: grey;
        }

        div.im-colormask {
            display: inline-block;
            border-style: inset;
            border-width: 2px;
            -webkit-appearance: textfield;
            -moz-appearance: textfield;
            appearance: textfield;
        }

        div.im-colormask > input {
            position: absolute;
            display: inline-block;
            background-color: transparent;
            color: transparent;
            -webkit-appearance: caret;
            -moz-appearance: caret;
            appearance: caret;
            border-style: none;
            left: 0; /*calculated*/
        }

        div.im-colormask > input:focus {
            outline: none;
        }

        div.im-colormask > input::-moz-selection {
            background: none;
        }

        div.im-colormask > input::selection {
            background: none;
        }

        div.im-colormask > input::-moz-selection {
            background: none;
        }

        div.im-colormask > div {
            color: black;
            display: inline-block;
            width: 100px; /*calculated*/
        }</style>
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body
        class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
@include('backend.layouts.header')
<!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>

    @include('backend.layouts.sidebar.side_bar')
    <!-- END: Left Aside -->
        @yield('content')
    </div>
    @include('backend.layouts.footer')
</div>

<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<script src="{{ asset('theme/js/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/form-repeater.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/toastr.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/summernote.js') }}" type="text/javascript"></script>
{{--<script src="{{ asset('theme/js/data-local.js') }}" type="text/javascript"></script>--}}

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

<script>
    @if(session()->has('success'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.success("{{ session()->get('success') }}", "Success");
    @php
        session()->forget('success');
    @endphp
    @endif

     @if(session()->has('failure'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('failure') }}", "Error");
    @php
        session()->forget('failure');
    @endphp
    @endif

    @if ($errors->any())
    @foreach ($errors->all() as $error)
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ $error }}", "Error");
    @endforeach
    @endif

    {{--@if(session()->has('success'))--}}
    {{--toastr.success("{{ session()->get('success') }}")--}}
    {{--@endif--}}

</script>
@yield('scripts')

</body>
</html>
