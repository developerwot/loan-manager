<html lang="en"
      class="wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n5-active wf-roboto-n6-active wf-roboto-n7-active wf-active">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <title>
        {{config('app.name')}}
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <script src="{{ asset('theme/js/webfont.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('theme/css/css.css') }}" media="all">
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset('theme/css/vendors.bundle.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme/css/style.bundle.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ asset('public/theme/icons/favicon.ico') }}">
    <script src="{{ asset('theme/js/jquery.mousewheel.min.js') }}"></script>
    <style type="text/css">span.im-caret {
            -webkit-animation: 1s blink step-end infinite;
            animation: 1s blink step-end infinite;
        }

        @keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        @-webkit-keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        span.im-static {
            color: grey;
        }

        div.im-colormask {
            display: inline-block;
            border-style: inset;
            border-width: 2px;
            -webkit-appearance: textfield;
            -moz-appearance: textfield;
            appearance: textfield;
        }

        div.im-colormask > input {
            position: absolute;
            display: inline-block;
            background-color: transparent;
            color: transparent;
            -webkit-appearance: caret;
            -moz-appearance: caret;
            appearance: caret;
            border-style: none;
            left: 0; /*calculated*/
        }

        div.im-colormask > input:focus {
            outline: none;
        }

        div.im-colormask > input::-moz-selection {
            background: none;
        }

        div.im-colormask > input::selection {
            background: none;
        }

        div.im-colormask > input::-moz-selection {
            background: none;
        }

        div.im-colormask > div {
            color: black;
            display: inline-block;
            width: 100px; /*calculated*/
        }</style>
    <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body
    class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div
        class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin"
        id="m_login">
        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m-login__logo">
                            <a href="javascript:void(0)">
                                <img src="{{ asset('images/logo.png') }}" width="50%">
                            </a>
                        </div>

                        @yield('content')

                    </div>
                </div>

            </div>
        </div>
        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content"
            style="background-image: url({{ asset('theme/images/bg-4.jpg') }})">
            <div class="m-grid__item m-grid__item--middle">
                <h3 class="m-login__welcome">
                   Laravel
                </h3>
                <p class="m-login__msg">
                    Lorem ipsum dolor sit amet, coectetuer adipiscing
                    <br>
                    elit sed diam nonummy et nibh euismod
                </p>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('theme/js/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/login.js') }}" type="text/javascript"></script>
<script src="{{ asset('theme/js/toastr.js') }}" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ $error }}", "Error");
    @endforeach
        @endif

        @if(session()->has('status'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.success("{{ session()->get('status') }}", "Success");
    @endif

        @if(session()->has('success'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.success("{{ session()->get('success') }}", "Success");
    @endif
        @if(session()->has('failure'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('failure') }}", "Failure");
    @endif
</script>
</body>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

@yield('scripts')
</html>
