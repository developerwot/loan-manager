@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/logo.png') }}" width="150px" height="80" border="0" alt=""/>
        @endcomponent
    @endslot
    @slot('subcopy')
        <br>
        Dear {{$user->name}},
        <br><br>
        <p> Please click on below button to reset your password !!</p><br>

        @component('mail::button', ['url' => $reset_link])
            Reset Password
        @endcomponent

        Thanks!<br>
        Laravel

    @endslot
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            ©{{ date('Y') }} Laravel. All rights reserved!!
        @endcomponent
    @endslot
@endcomponent



