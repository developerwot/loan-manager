<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/dashboard');
    } else {
        return redirect('/login');
    }
});

Route::get('/home', function () {
    return redirect('/dashboard');
});

/*
   |--------------------------------------------------------------------------
   | Routes Without authentication
   |--------------------------------------------------------------------------
 */
Route::get('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password_reset_save');
Route::post('/reset/password', 'Auth\ResetPasswordController@reset')->name('password.reset');


Auth::routes();

Route::group(['middleware' => 'auth'], function () {


    Route::group(['namespace' => 'Auth'], function () {

        Route::get('update-profile', 'ProfileController@editProfile')->name('edit.profile');
        Route::post('update-profile', 'ProfileController@updateProfile')->name('update.profile');

        Route::get('logout', 'LoginController@logout')->name('logout');
    });

    /*
      |--------------------------------------------------------------------------
      | Super Admin Routes
      |--------------------------------------------------------------------------
    */
    Route::group(['namespace' => 'Backend'], function () {

        //After Login redirect to dashboard
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::resource('users', 'UserController');
        Route::get('users/delete/{id}', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);

        Route::resource('roles', 'RoleController');
        Route::get('roles/delete/{id}', ['as' => 'roles.delete', 'uses' => 'RoleController@destroy']);

        //Custom Module
        Route::resource('clients', 'ClientController');
        Route::get('clients/delete/{id}', ['as' => 'clients.delete', 'uses' => 'ClientController@destroy']);

        Route::resource('transactions', 'TransactionController');
        Route::get('transactions/delete/{id}', ['as' => 'transactions.delete', 'uses' => 'TransactionController@destroy']);

        Route::get('client_transactions/index', ['as' => 'client_transactions.index', 'uses' => 'ClientTransactionController@index']);
        Route::get('client_transactions/create/{transaction_id}', ['as' => 'client_transactions.create', 'uses' => 'ClientTransactionController@create']);
        Route::post('client_transactions/create/{transaction_id}', ['as' => 'client_transactions.store', 'uses' => 'ClientTransactionController@store']);
        Route::get('client_transactions/edit/{transaction_id}', ['as' => 'client_transactions.edit', 'uses' => 'ClientTransactionController@edit']);
        Route::put('client_transactions/update/{transaction_id}', ['as' => 'client_transactions.update', 'uses' => 'ClientTransactionController@update']);
       //Route::get('client_transactions/delete/{id}', ['as' => 'transactions.delete', 'uses' => 'ClientTransactionController@destroy']);

        Route::get('interest_cron',['as'=>'interest_cron','uses'=>'TransactionCronController@add_pending_interests']);
    });
});


