<?php

use Illuminate\Database\Seeder;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \App\models\Transaction::truncate();
        \App\models\ClientTransaction::truncate();

    }
}
