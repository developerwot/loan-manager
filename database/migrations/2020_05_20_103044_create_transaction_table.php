<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->float('amount');
            $table->enum('with_interest',['Yes','No']);
            $table->decimal('interest_rate',3,2)->nullable();
            $table->date('transaction_date');
            $table->enum('status',['Ongoing','Completed']);
            $table->enum('payment_mode',['Cash','Bank Transfer']);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
