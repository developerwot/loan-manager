<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'user_id',
        'amount',
        'pending_amount',
        'with_interest',
        'interest_rate',
        'dom_from',
        'dom_to',
        'transaction_date',
        'status',
        'payment_mode',
        'note',
    ];
    public function client()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function client_transaction()
    {
        return $this->hasMany('App\Models\ClientTransaction', 'transaction_id')->orderBy('pending_payment_date','DESC');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($transaction) { // before delete() method call this
            $transaction->client_transaction()->delete();
            // do the rest of the cleanup...
        });
    }
}
