<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTransaction extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'transaction_id',
        'amount',
        'payment_mode',
        'payment_type',
        'pending_payment_date',
        'payment_date',
        'payment_status',
        'note'
    ];
    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id');
    }
}
