<?php

namespace App\Repositories\Backend\User;

interface UserContract
{

    public function getAll();

    public function getAllClients($user_role='client');

    public function find($id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
