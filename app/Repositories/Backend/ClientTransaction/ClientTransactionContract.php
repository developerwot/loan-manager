<?php

namespace App\Repositories\Backend\ClientTransaction;

interface ClientTransactionContract
{

    public function getAll();

    public function find($id);

    public function findByStatus($status,$id);

    public function getLastClientTransaction($type,$transaction_id);

    public function getPendingInterestByTransaction($transaction_id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
