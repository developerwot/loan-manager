<?php

namespace App\Repositories\Backend\ClientTransaction;

use App\EventParticipate;
use App\models\ClientTransaction;
use App\Repositories\Backend\ClientTransaction\ClientTransactionContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class ClientTransactionRepository implements ClientTransactionContract
{

    protected $model;

    public function __construct(ClientTransaction $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $trans_data = $this->model->with('transaction')->orderBy('id', 'desc')->latest()->get();
        return $trans_data;
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
    public function findByStatus($status,$id)
    {
        return $this->model->where('payment_status','Pending')->where('id',$id)->first();
    }

     public function getLastClientTransaction($type,$transaction_id)
    {
        return $this->model->where('payment_type',$type)->where('transaction_id',$transaction_id)->latest()->first();
    }
    public function getPendingInterestByTransaction($transaction_id)
    {
        return $this->model->where('transaction_id',$transaction_id)
                    ->where('payment_type','Interest Amount')
                    ->where('payment_status','Pending')
                    ->sum('amount');
    }

    public function store($input)
    {
        $transaction = $this->model->create($input);

        return $transaction;
    }

    public function update($id, $input)
    {
        $transaction = $this->model->findOrFail($id);
        return $transaction->update($input);
    }

    public function delete($id)
    {
        $user = $this->model->findOrFail($id);
        $user->destroy($id);
    }

}
