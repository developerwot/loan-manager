<?php namespace App\Repositories\Backend\Profile;

interface ProfileContracts
{

	public function edit();

	public function update($data);

}
