<?php

namespace App\Repositories\Backend\Transaction;

use App\EventParticipate;
use App\models\Transaction;
use App\Repositories\Backend\Transaction\TransactionContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class TransactionRepository implements TransactionContract
{

    protected $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $trans_data = $this->model->with('client')->with('client_transaction')->orderBy('id', 'desc')->latest()->get();
        return $trans_data;
    }

    public function find($id)
    {
        return $this->model->with('client')->with('client_transaction')->find($id);
    }

    public function store($input)
    {
        $transaction = $this->model->create($input);

        return $transaction;
    }

    public function update($id, $input)
    {
        $transaction = $this->model->findOrFail($id);

        return $transaction->update($input);
    }

    public function delete($id)
    {
        $user = $this->model->findOrFail($id);
        $user->destroy($id);
    }

}
