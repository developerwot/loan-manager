<?php

namespace App\Repositories\Backend\Transaction;

interface TransactionContract
{

    public function getAll();

    public function find($id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
