<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
	protected $table = 'roles';

	protected $fillable = ['name', 'display_name', 'description'];

	public function scopeNotAdmins($query)
	{
		return $query->where('name','<>','admin');
	}
    public function user()
    {
        return $this->belongsToMany('App\User', 'role_user', 'user_id', 'role_id');
    }
}
