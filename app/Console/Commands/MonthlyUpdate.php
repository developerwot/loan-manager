<?php

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use App\models\ClientTransaction;
use App\models\Transaction;
use function GuzzleHttp\Psr7\str;
use Illuminate\Console\Command;
use Log;

class MonthlyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'month:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly Update Interest payment to pending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $controller = new Controller();
            $active_transactions = Transaction::with('client_transaction')
                                    ->where('status','Ongoing')
                                    ->where('with_interest','Yes')
                                    ->get();
            $interest_amount_month =0;
            $interest_amount =0;
            $current_date  = date('Y-m-1');
            // $current_date  = date('Y-m-01',strtotime("+1 month")); // Only check for 1st date of month
            foreach ( $active_transactions as $transaction ) {
                $count_month = $controller->getMonthsCountBetweenDates($transaction->transaction_date,$current_date);
                $client_transaction_amount = ClientTransaction::
                where('transaction_id',$transaction->id)
                    ->where('payment_type','Primary Amount')->sum('amount');
                if(!empty($client_transaction_amount)) {
                    $transaction_amt = $transaction->amount - $client_transaction_amount;
                }
                else {
                    $transaction_amt = $transaction->amount;
                }
                $interest_amount_month = ($transaction_amt * $transaction->interest_rate) / 100;
                if($count_month > 0) {
                    if ($count_month == 1) {
                        //Calculate day wise interest
                        $days_count = intval($controller->dateDiffInDays($transaction->transaction_date, $current_date));
                        $interest_amount_days = ($interest_amount_month * $days_count) / 30;

                        //Check if entry exists
                        $c_transaction = ClientTransaction::where('transaction_id', $transaction->id)->whereMonth('pending_payment_date', date('m', strtotime($current_date)))->get();
                        if ($c_transaction->isEmpty()) {
                            $client_transaction = [];
                            $client_transaction['transaction_id'] = $transaction->id;
                            $client_transaction['amount'] = $interest_amount_days;
                            $client_transaction['payment_type'] = 'Interest Amount';
                            $client_transaction['pending_payment_date'] = date("Y-m-1");
                            $client_transaction['payment_date'] = date("Y-m-1");
                            $client_transaction['payment_status'] = 'Pending';
                            $client_transaction['note'] = 'Pending Interest record auto generated for '.$days_count.' days';
                            $client_trans = ClientTransaction::create($client_transaction);
                        }
                    } else {

                        $pending_interests = $controller->checkPendingInterestsMonth($transaction);
                        $check_current_date = date("d",strtotime($transaction->transaction_date));
                        if(!empty($pending_interests)) {
                            foreach ($pending_interests as $key=>$pending_int) {
                                if( $key==1 && $check_current_date > 1) {
                                    //Check if entry exists
                                    $c_transaction = ClientTransaction::where('transaction_id', $transaction->id)->whereMonth('pending_payment_date', date('m', strtotime($current_date)))->get();
                                    if ($c_transaction->isEmpty()) {

                                        $days_count = intval($controller->dateDiffInDays($transaction->transaction_date,date("Y-m-d",strtotime('01-'.$pending_int) )));
                                        echo $days_count.'<hr>';
                                        $interest_amount_days = ($interest_amount_month * $days_count) / 30;
                                        echo $interest_amount_days.'<hr>';
                                        $client_transaction = [];
                                        $client_transaction['transaction_id'] = $transaction->id;
                                        $client_transaction['amount'] = $interest_amount_days;
                                        $client_transaction['payment_type'] = 'Interest Amount';
                                        $client_transaction['pending_payment_date'] = date("Y-m-d",strtotime('01-'.$pending_int));
                                        $client_transaction['payment_date'] = date("Y-m-d",strtotime('01-'.$pending_int));
                                        $client_transaction['payment_status'] = 'Pending';
                                        $client_transaction['note'] = 'Pending Interest record auto generated for '.$days_count.' days';
                                        $client_trans = ClientTransaction::create($client_transaction);
                                    }

                                }
                                else {
                                    //echo $pending_int.'<hr>';
                                    $client_transaction = [];
                                    $client_transaction['transaction_id'] = $transaction->id;
                                    $client_transaction['amount'] = $interest_amount_month;
                                    $client_transaction['payment_type'] = 'Interest Amount';
                                    $client_transaction['pending_payment_date'] = date("Y-m-d",strtotime('01-'.$pending_int));
                                    $client_transaction['payment_date'] = date("Y-m-d",strtotime('01-'.$pending_int));
                                    $client_transaction['payment_status'] = 'Pending';
                                    $client_transaction['note'] = 'Pending Interest record auto generated monthly on '.$client_transaction['pending_payment_date'];
                                    $client_trans = ClientTransaction::create($client_transaction);
                                }

                            }
                        }

                    }
                }
            }
            //dd($current_date);

        } catch (\Exception $e) {

            Log::error('Transaction cron error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            //return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }
}
