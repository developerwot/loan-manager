<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\models\ClientTransaction;
use App\models\Transaction;
use App\Repositories\Backend\Dashboard\DashboardContract;
use App\Repositories\Backend\User\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    protected $repository;

    public function __construct(DashboardContract $repository)
    {
        $this->repository = $repository;
    }

    //return all the reservation record from database
    public function index()
    {
        try {

            DB::enableQueryLog();
            $day = date("d");
            $transactions = Transaction::with('client')->where('status','Ongoing')
                                        ->get();

            $user_role = 'client';
            $user_count = $users_data = User::whereHas('roles', function($q) use($user_role){
                if(!empty($user_role)) {
                    $q->whereIn('name',[$user_role]);
                }

            })->count();
            return view('backend.dashboard', compact('user_count','transactions'));

        } catch (\Exception $e) {
            Log::error('Dashboard error:' . $e->getMessage());
           // return redirect()->route('dashboard')->with('failure', 'Something went wrong');

        }
    }


}
