<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ClientTransactionRequest;
use App\Http\Requests\Backend\ClientTransactionUpdateRequest;
use App\models\ClientTransaction;
use App\models\Transaction;
use App\Repositories\Backend\ClientTransaction\ClientTransactionContract;
use App\Repositories\Backend\User\UserContract;
use App\User;
use Illuminate\Support\Facades\Log;

class ClientTransactionController extends Controller
{
    protected $repository;
    protected $user_repository;

    public function __construct(ClientTransactionContract $repository,UserContract $userRepository)
    {
        $this->repository = $repository;
        $this->user_repository = $userRepository;
    }

    public function index()
    {

        try {
            $client_transactions = $this->repository->getAll();

            $client_transaction = $client_transactions->filter(function ($item){

                if(!empty($item->transaction)) {
                    $item->client_name = User::where('id',$item->transaction->user_id)->first()->name;

                }

                return $item->transaction_date = date('d-m-Y',strtotime($item->pending_payment_date));
            });

            return view('backend.client_transactions.index', compact('client_transactions'));

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }

    public function create($transaction_id)
    {

        if(!empty($transaction_id)) {
            $transaction = Transaction::find($transaction_id);
            $pending_interests_amount = 0;
            if($transaction->with_interest == 'Yes') {
                $pending_interests_amount = $this->repository->getPendingInterestByTransaction($transaction_id);
            }

            return view('backend.client_transactions.form', compact('transaction_id','transaction'));
        }
        else {
            return redirect()->route('transactions.index')->with('failure', 'Transaction Not Found');
        }

    }

    public function store(ClientTransactionRequest $request,$transaction_id)
    {
        try {

            $input = $request->only('transaction_id', 'amount', 'payment_mode','payment_type','note','payment_date');
            $input['payment_date'] = date("Y-m-d",strtotime($input['payment_date']));
            $input['pending_payment_date'] = date("Y-m-d",strtotime($input['payment_date']));
            $transaction = Transaction::find($transaction_id);
            if($input['amount'] > $transaction->pending_amount ) {
                return redirect()->route('transactions.show',$transaction_id)->with('failure', 'Payment amount is larger than pending amount');
            }

            //Find Last client date for paid primary amount
            $last_client_transaction = $this->repository->getLastClientTransaction('Primary Amount',$transaction_id);
            if(!empty($last_client_transaction)) {
                $last_client_transaction_date = $last_client_transaction->payment_date;
            }

            $ctransaction = $this->repository->store($input);
            $pending_interests_amount = 0;

            //Update Pending Amount for Parimary Payment
            if( $input['payment_type'] == 'Primary Amount' ) {
                $last_pending_amount = $transaction->pending_amount;
                $transaction->pending_amount = $transaction->pending_amount - $input['amount'];
                $transaction->update();

                //Check and make pending interest entries for last date
               // echo $pending_amount.'<hr>';
               // echo $pending_interests_amount.'<hr>';

                //Check For last Month Interest
                $current_date = $input['payment_date']; //Consider current month date
                $current_month_start = date('Y-m-1',strtotime($input['payment_date']));
                if(empty($last_client_transaction_date)) {

                    $days_count = intval($this->dateDiffInDays($current_date,$current_month_start));
                }
                else if(date('m-Y',strtotime($last_client_transaction_date)) == date('m-Y',strtotime($current_month_start))) {
                    $days_count = intval($this->dateDiffInDays($current_date,$last_client_transaction_date));
                }
                else {
                    $days_count = intval($this->dateDiffInDays($current_date,$current_month_start));
                }

                $interest_amount_month = ($last_pending_amount * $transaction->interest_rate) / 100;
                $interest_amount_days = ($interest_amount_month * $days_count) / 30;
                if(!empty($interest_amount_days)) {

                    $client_transaction = [];
                    $client_transaction['transaction_id'] = $transaction->id;
                    $client_transaction['amount'] = $interest_amount_days;
                    $client_transaction['payment_type'] = 'Interest Amount';
                    $client_transaction['pending_payment_date'] = $current_date;
                    $client_transaction['payment_date'] = $current_date;
                    $client_transaction['payment_status'] = 'Pending';
                    $client_transaction['note'] = 'Pending Interest record auto generated for '.$days_count.' days';
                    $client_trans = $this->repository->store($client_transaction);
            }

           $pending_interests_amount = $this->repository->getPendingInterestByTransaction($transaction_id);


            if($transaction->pending_amount == 0) {

                //Make Completed Transaction
                if($transaction->with_interest == 'No') {
                    echo "12<br>";
                    $transaction->status = 'Completed';
                    $transaction->update();
                }
                else if ($transaction->with_interest == 'Yes') {
                    if($pending_interests_amount == 0 ) {
                        $transaction->status = 'Completed';
                        $transaction->update();
                    }
                 }
                }
            }
            else {
                $pending_interests_amount = $this->repository->getPendingInterestByTransaction($transaction_id);
                if($pending_interests_amount == 0 ) {
                    $transaction->status = 'Completed';
                    $transaction->update();
                }
            }

            return redirect()->route('transactions.show',$transaction_id)->with('success', 'New Client Payment added successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }


    public function edit($id)
    {

        try {
            $client_transactions = $this->repository->find($id);
            $transaction = Transaction::find($client_transactions->transaction_id);
            $pending_interests_amount = $this->repository->getPendingInterestByTransaction($client_transactions->transaction_id);
            return view('backend.client_transactions.form', compact('client_transactions','transaction','pending_interests_amount'));

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('client_transactions.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(ClientTransactionUpdateRequest $request, $id)
    {
        try {
            $input = $request->only('transaction_id',  'payment_mode','payment_type','note','payment_date');
            $input['payment_date'] = date("Y-m-d",strtotime($input['payment_date']));
            $input['payment_status']  = 'Paid';

            $transaction = Transaction::find($input['transaction_id']);
            $this->repository->update($id, $input);
            $pending_interests_amount = $this->repository->getPendingInterestByTransaction($input['transaction_id']);
            if( $pending_interests_amount == 0  && $transaction->pending_amount == 0 ) {
                $transaction->status = 'Completed';
                $transaction->update();
            }
            return redirect()->route('transactions.show',$input['transaction_id'])->with('success', 'Client Transaction updated successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('client_transactions.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('client_transactions.index')->with('success', 'Transaction deleted successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('client_transactions.index')->with('failure', 'Something went wrong');
        }
    }
}
