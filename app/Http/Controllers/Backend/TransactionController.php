<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\TransactionRequest;
use App\Http\Controllers\Controller;
use App\models\ClientTransaction;
use App\Repositories\Backend\Transaction\TransactionContract;
use App\Repositories\Backend\User\UserContract;
use App\Role;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    protected $repository;
    protected $user_repository;

    public function __construct(TransactionContract $repository,UserContract $userRepository)
    {
        $this->repository = $repository;
        $this->user_repository = $userRepository;
    }

    public function index()
    {
        try {
            $transactions_data = $this->repository->getAll();
            $transactions = $transactions_data->filter(function ($item){
                 $item->name = $item->client->name;
                 unset($item->client);
                 $item->interest_rate = intval($item->interest_rate).' %';
                 $pending_interests_amount = ClientTransaction::
                            where('transaction_id',$item->id)
                                ->where('payment_type','Interest Amount')
                                ->where('payment_status','Pending')
                                ->sum('amount');
                if($item->pending_amount == 0 )
                {
                    $item->pending_amount = $pending_interests_amount;
                }
                else {
                    $item->pending_amount = $item->pending_amount + $pending_interests_amount;
                }

                unset($item->client_transaction);
                /*if($item->status == 'Ongoing') {

                    $pending_interests = $this->checkPendingInterestsMonth($item);

                    if ($item->due_amount == 0) {

                        if ($item->with_interest == 'No') {
                            $item->status = 'Completed';
                            $item->update();
                        } else if ($item->with_interest == 'Yes' && empty($pending_interests)) {
                            $item->status = 'Completed';
                            $item->update();
                        }
                    }
                }*/
                 return $item->transaction_date = date('d-m-Y',strtotime($item->transaction_date));
            });
            return view('backend.transactions.index', compact('transactions'));

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('/')->with('failure', 'Something went wrong');
        }
    }

    public function create()
    {
        $clients  = $this->user_repository->getAllClients('client');
        return view('backend.transactions.form', compact('clients'));
    }

    public function store(TransactionRequest $request)
    {
        try {
            $input = $request->only('user_id','transaction_date', 'amount', 'with_interest','interest_rate','payment_mode','dom_from','dom_to','note');
            $input['transaction_date'] = date('Y-m-d',strtotime($input['transaction_date']));
            $input['pending_amount'] = $input['amount'];
            $input['status'] = 'Ongoing';
            if(!empty($input['with_interest']) && $input['with_interest'] == 'No') {
                $input['interest_rate'] = '0';
                $input['dom_from'] = '0';
                $input['dom_to'] = '0';
            }

            $transaction = $this->repository->store($input);
            return redirect()->route('transactions.index')->with('success', 'New Transaction added successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }

    public function edit($id)
    {
        try {
            $transaction = $this->repository->find($id);
            $clients  = $this->user_repository->getAllClients('client');
            return view('backend.transactions.form', compact('transaction','clients'));

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }
    public function show($id)
    {
        try {

            $transaction = $this->repository->find($id);
            $pending_interests_amount = 0;
            if(!empty($transaction)) {
                if($transaction->status == 'Ongoing') {
                    $client_transaction_amount = ClientTransaction::
                                    where('transaction_id',$id)
                                    ->where('payment_type','Primary Amount')->sum('amount');

                    $pending_amount = $transaction->amount - $client_transaction_amount;

                    $pending_interests_amount = ClientTransaction::
                                    where('transaction_id',$id)
                                        ->where('payment_type','Interest Amount')
                                        ->where('payment_status','Pending')
                                        ->sum('amount');


                    if($transaction->pending_amount == 0) {

                        if ($transaction->with_interest == 'No') {
                            $transaction->status = 'Completed';
                            $transaction->update();
                        } else if ($transaction->with_interest == 'Yes' && $pending_interests_amount == 0) {
                            $transaction->status = 'Completed';
                            $transaction->update();
                        }
                    }
                }
            }

            $interest_month_count = 0;
            $interest_amount =0;
            return view('backend.transactions.view', compact('transaction','interest_amount','pending_interests_amount'));

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }

    public function update(TransactionRequest $request, $id)
    {

        try {
            $input = $request->only('user_id','transaction_date', 'amount', 'with_interest','interest_rate','payment_mode','date_from_to','note');
            $input['transaction_date'] = date('Y-m-d',strtotime($input['transaction_date']));
            $input['pending_amount'] = $input['amount'];
            $input['status'] = 'Ongoing';
            if(!empty($input['with_interest']) && $input['with_interest'] == 'No') {
                $input['interest_rate'] = '0';
                $input['dom_from'] = '0';
                $input['dom_to'] = '0';

            }
            $this->repository->update($id, $input);
            return redirect()->route('transactions.index')->with('success', 'Transaction updated successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('transactions.index')->with('Something went wrong');
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return redirect()->route('transactions.index')->with('success', 'Transaction deleted successfully');

        } catch (\Exception $e) {
            Log::error('Transaction management error:' . $e->getMessage());
            return redirect()->route('transactions.index')->with('failure', 'Something went wrong');
        }
    }
}
