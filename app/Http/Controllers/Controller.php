<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getMonthsCountBetweenDates($date1,$date2) {
        /*$date1 = '2000-12-25';
        $date2 = '2001-02-20';*/

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        return $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
    }
    public function getAllMonthsBwetweenDates($date1,$date2)
    {
        $monthArray = [];
        $start    = (new \DateTime($date1))->modify('first day of this month');
        $end      = (new \DateTime($date2))->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key=>$dt) {
            if($key == 0)  continue;
            $monthArray[$key]= $dt->format("m-Y");
        }

        return $monthArray;
    }
    public function checkPendingInterestsMonth($transaction)
    {
             $pending_months_array  = [];

            /*Calculate Interest*/
            $count_month_pay = $this->getMonthsCountBetweenDates($transaction->transaction_date,date('Y-m-d'));
            $pending_months_array = $this->getAllMonthsBwetweenDates($transaction->transaction_date,date('Y-m-d'));

            if(empty($count_month_pay)) {
                /*No Interest need to pay*/
                return 0;
            }
            else  {

                //$current_date = date("d",strtotime(date("10-04-2020"))); //Set a Custom date for check logic
                $current_date = date("d");
                $transaction_date_from = $transaction->dom_from;
                $transaction_date_to = $transaction->dom_to;

                if( $current_date >= $transaction_date_from && $current_date <= $transaction_date_to ) {

                    /*Current Date Between Interest Time period */
                    if ($key = array_search(date('m-Y'), $pending_months_array,false)) {
                        unset($pending_months_array[$key]);
                    }
                }
                if(!empty($transaction->client_transaction)) {

                    foreach ($transaction->client_transaction as $ctrans) {
                        if ($ctrans->payment_type == 'Interest Amount' && in_array(date("m-Y",strtotime($ctrans->pending_payment_date)),$pending_months_array)) {
                            if ($key = array_search(date("m-Y",strtotime($ctrans->pending_payment_date)), $pending_months_array,false)) {

                                unset($pending_months_array[$key]);
                            }
                        }

                    }
                }

                return $pending_months_array;
            }

    }
    public function dateDiffInDays($date1, $date2)
    {
        // Calulating the difference in timestamps
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return abs(round($diff / 86400));
    }
}
