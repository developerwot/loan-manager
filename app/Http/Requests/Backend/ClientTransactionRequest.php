<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ClientTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'transaction_id' => 'required',
            'amount' => 'required',
            'payment_mode' => 'required',
            'payment_type' => 'required',
            'payment_date' => 'required',
            'note' => 'required',

        ];
    }
}
